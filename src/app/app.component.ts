import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'task';
  currentView: string = 'Table'
  tableData: Array<any> = [];
  constructor(private http: HttpClient) {

  }
  ngOnInit() {
    // here i am getting the csv file from asset folder
    this.readCsvFromAsset();
  }
  readCsvFromAsset() {
    this.http.get('assets/Data.csv', { responseType: 'text' })
      .subscribe((data: any) => {
        // after read the csv file i am converting the text data in to array of object
        // i split the data individually using comma 
        this.csvtojson(data);
      },
        error => {
          console.log(error);
          alert('Error While Getting Csv From Asset');
        }
      );
  }
  csvtojson(csv) {
    var lines = csv.split("\n");
    // here i am split the header first
    var headers = lines[0].split(",");

    for (var i = 1; i < lines.length; i++) {

      var obj = {};
      var currentline = lines[i].split(",");

      for (var j = 0; j < headers.length; j++) {
      //  In this loop i split the data and assign the data in to the header like key and value
        obj[headers[j]] = currentline[j];
      }
      // Here I am push the data to the tabledata property
      this.tableData.push(obj);
    }
  }

  changeView() {
    // when user click the switch view button so that time i check current view if the current view is table means i just changed to card also same in card view
    if (this.currentView === 'Table') {
      this.currentView = 'Card';
    } else {
      this.currentView = 'Table';
    }
  }
}
